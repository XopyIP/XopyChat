package me.xopyip.chat;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by XopyIP on 2015-05-02.
 */
public class Client {
    private static Socket socket;

    public static void main(String[] args) throws Exception{
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Podaj IP:");
        String ip = console.readLine();
        System.out.println("Podaj port:");
        int port = Integer.parseInt(console.readLine());
        System.out.println("Podaj nick:");
        String nick = console.readLine();
        socket = new Socket(ip, port);
        PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
        final BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer.println("nick:" + nick);
        Thread readThread = new Thread(new Runnable(){

            public void run() {
                try {
                    String msg;
                    while ((msg = in.readLine()) != null) {
                        System.out.println(msg);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        readThread.setDaemon(true);
        readThread.start();
        while (true){
            String s = console.readLine();
            writer.println(s);
        }
    }
}
