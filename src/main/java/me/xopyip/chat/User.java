package me.xopyip.chat;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by XopyIP on 2015-05-02.
 */
public class User {
    private Socket s;
    private String nickName;

    private PrintWriter writer;
    public User(Socket s, String nickName) {
        try {
            writer = new PrintWriter(s.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.s = s;
        this.nickName = nickName;
    }

    public Socket getS() {
        return s;
    }

    public String getNickName() {
        return nickName;
    }

    public void send(String msg) {
        writer.println(msg);
    }
}
