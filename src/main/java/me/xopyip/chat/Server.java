package me.xopyip.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by XopyIP on 2015-05-02.
 */
public class Server {
    public static final int PORT = 9999;
    private ServerSocket socket;
    private static Server instance;

    private Set<User> users = new HashSet<User>();

    public Server() {
        instance = this;
        System.out.println("Uruchamianie serwera...");
        try {
            socket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Rozpoczynanie nas�uchu na porcie " + PORT);
        Thread connectionThread = new Thread(new ConnectionRunnable(socket));
        connectionThread.setDaemon(false);
        connectionThread.start();

    }
    public static void main(String[] args){
        new Server();
    }
    public static Server getInstance(){
        return instance;
    }
    public void addClient(final Socket s, String nickName){
        System.out.println(s.getInetAddress().getHostAddress() + " " + nickName + " connected!");
        final User u = new User(s, nickName);
        users.add(u);
        u.send("Witamy na chacie");
        u.send("Mi�ych rozm�w ;)");
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    final BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    String msg;
                    while ((msg = in.readLine()) != null) {
                        Server.getInstance().sendToAllClients(u.getNickName() + ": " + msg);
                    }
                } catch(SocketException e){
                    Server.getInstance().removeClient(u);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    private void sendToAllClients(String msg) {
        System.out.println(msg);
        for(User u : users){
            u.send(msg);
        }
    }

    public void removeClient(User u){
        sendToAllClients(u.getNickName() + " disconnected!");
        users.remove(u);
    }
}
