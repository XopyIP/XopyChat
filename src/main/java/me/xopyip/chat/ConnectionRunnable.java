package me.xopyip.chat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by XopyIP on 2015-05-02.
 */
public class ConnectionRunnable implements Runnable {
    private ServerSocket socket;

    public ConnectionRunnable(ServerSocket socket) {

        this.socket = socket;
    }

    public void run() {
        while(true) {
            Socket s = null;
            try {
                s = socket.accept();
                BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
                String str;
                if((str = reader.readLine()) != null) {
                    if(str.contains("nick:")){
                        Server.getInstance().addClient(s, str.split(":")[1]);
                    }else{
                        s.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
